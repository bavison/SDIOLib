/* Copyright (c) 2021 RISC OS Developments Ltd
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "swis.h"

#include "Global/Keyboard.h"
#include "Global/RISCOS.h"
#include "Interface/SDIO.h"

#include "FakeLibIntHdr.h"
#include "main.h"
#include "key.h"

static bool on_vector;
static uint8_t bus, slot;

/** Initialise the KeyV routine that fakes SDIO card interrupts */
void key_init(void)
{
  /* Since faking an interrupt isn't a feature exposed by SDIOLib, we're
   * going to need to go behind its back and call SDIODriver SWIs instead.
   * To do that, we need the bus and slot numbers. These aren't exposed by
   * SDIOLib either, but we can assume that the first IO card found by an
   * SDIOLib enumeration will correspond to the first IO card found by an
   * SDIODriver enumeration. */
  for (uint32_t slot_key = 0;;)
  {
    uint32_t slot_details;
    if (_swix(SDIO_Enumerate, _INR(0,1)|_OUTR(1,2),
        SDIOEnumerate_Slots, slot_key,
        &slot_key, &slot_details) != NULL ||
        slot_key == 0)
      break;

    for (uint32_t unit_key = 0;;)
    {
      uint32_t unit_details;
      if (_swix(SDIO_Enumerate, _INR(0,2)|_OUT(1)|_OUT(3),
          SDIOEnumerate_Units, unit_key, slot_details,
          &unit_key, &unit_details) != NULL ||
          unit_key == 0)
        break;

      if ((unit_details & SDIOEnumerate_UnitMask) == 0)
        continue; /* ignore any memory units */

      bus = (slot_details & SDIOEnumerate_BusMask) >> SDIOEnumerate_BusShift;
      slot = (slot_details & SDIOEnumerate_SlotShift) >> SDIOEnumerate_SlotShift;
      _swix(OS_Claim, _INR(0,2), KEYV, keyv_veneer, module_pw);
      on_vector = true;
      return;
    }
  }
}

/** Shut down the KeyV routine that fakes SDIO card interrupts */
void key_final(void)
{
  if (on_vector)
    _swix(OS_Release, _INR(0,2), KEYV, keyv_veneer, module_pw);
}

/** Called on KeyV, fakes an IO card interrupt when right-Shift pressed */
int keyv_handler(_kernel_swi_regs *r, void *pw)
{
  (void) pw;
  if (r->r[0] == KeyV_KeyDown &&
      r->r[1] == KeyNo_ShiftRight)
  {
    _swix(SDIO_Control, _INR(0,2),
        SDIOControl_FakeSDIOCardInt,
        (bus << SDIOControl_BusShift) | (slot << SDIOControl_SlotShift),
        1);
  }
  return 1; // always pass on
}
