# Copyright (c) 2021 RISC OS Developments Ltd
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# Makefile for SDIO glue library

COMPONENT ?= SDIOLib
LIBRARIES  = ${LIBRARYZM}
LIBRARY    = unused
LIBRARYZM  = ${TARGET}
VPATH      = include${SEP}linux${SEP}mmc sys${SEP}dev${SEP}sdmmc
HDRS       = card sdio_func sdmmcvar pm
OBJS       = common common_asm sdio_io sdmmc_io
CFLAGS    += ${C_NOWARN_NON_ANSI_INCLUDES}
CINCLUDES  = -I${EXPDIR}${SEP}${TARGET}
SOURCES_TO_SYMLINK = $(wildcard include/linux/mmc/h/*) sys/dev/sdmmc/h/sdmmcvar

include CLibrary

ifeq (,${SUFFIX_HEADER})
SUBDIR_HEADER = .h
endif

card.exphdr: card.h
	${MKDIR} ${EXPDIR}${SEP}linux${SEP}mmc${SUBDIR_HEADER}
	${CP} $^ ${EXPDIR}${SEP}linux${SEP}mmc${SUBDIR_HEADER}${SEP}$*${SUFFIX_HEADER} ${CPFLAGS}

pm.exphdr: pm.h
	${MKDIR} ${EXPDIR}${SEP}linux${SEP}mmc${SUBDIR_HEADER}
	${CP} $^ ${EXPDIR}${SEP}linux${SEP}mmc${SUBDIR_HEADER}${SEP}$*${SUFFIX_HEADER} ${CPFLAGS}

sdio_func.exphdr: sdio_func.h
	${MKDIR} ${EXPDIR}${SEP}linux${SEP}mmc${SUBDIR_HEADER}
	${CP} $^ ${EXPDIR}${SEP}linux${SEP}mmc${SUBDIR_HEADER}${SEP}$*${SUFFIX_HEADER} ${CPFLAGS}

sdmmcvar.exphdr: sdmmcvar.h
	${MKDIR} ${EXPDIR}${SEP}sys${SEP}dev${SEP}sdmmc${SUBDIR_HEADER}
	${CP} $^ ${EXPDIR}${SEP}sys${SEP}dev${SEP}sdmmc${SUBDIR_HEADER}${SEP}$*${SUFFIX_HEADER} ${CPFLAGS}

# Dynamic dependencies:
